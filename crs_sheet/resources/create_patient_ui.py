# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/dmj/fizmed/mdovgialo/crs_sheet/crs_sheet/resources/create_patient.ui',
# licensing of '/dmj/fizmed/mdovgialo/crs_sheet/crs_sheet/resources/create_patient.ui' applies.
#
# Created: Wed Oct  9 14:19:00 2019
#      by: pyside2-uic  running on PySide2 5.11.0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_patient_create_window(object):
    def setupUi(self, patient_create_window):
        patient_create_window.setObjectName("patient_create_window")
        patient_create_window.resize(400, 300)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(patient_create_window.sizePolicy().hasHeightForWidth())
        patient_create_window.setSizePolicy(sizePolicy)
        self.label = QtWidgets.QLabel(patient_create_window)
        self.label.setGeometry(QtCore.QRect(10, 10, 91, 31))
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(patient_create_window)
        self.label_2.setGeometry(QtCore.QRect(10, 50, 191, 21))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(patient_create_window)
        self.label_3.setGeometry(QtCore.QRect(10, 80, 151, 21))
        self.label_3.setObjectName("label_3")
        self.birth_date = QtWidgets.QSpinBox(patient_create_window)
        self.birth_date.setGeometry(QtCore.QRect(210, 50, 50, 22))
        self.birth_date.setMinimum(1950)
        self.birth_date.setMaximum(2050)
        self.birth_date.setProperty("value", 2000)
        self.birth_date.setObjectName("birth_date")
        self.label_4 = QtWidgets.QLabel(patient_create_window)
        self.label_4.setGeometry(QtCore.QRect(10, 130, 111, 31))
        self.label_4.setObjectName("label_4")
        self.ethiology_main = QtWidgets.QComboBox(patient_create_window)
        self.ethiology_main.setGeometry(QtCore.QRect(170, 130, 81, 31))
        self.ethiology_main.setObjectName("ethiology_main")
        self.ethiology_main.addItem("")
        self.ethiology_main.addItem("")
        self.ethiology_main.addItem("")
        self.ethiology_custom = QtWidgets.QPlainTextEdit(patient_create_window)
        self.ethiology_custom.setGeometry(QtCore.QRect(260, 130, 91, 31))
        self.ethiology_custom.setObjectName("ethiology_custom")
        self.patient_id = QtWidgets.QPlainTextEdit(patient_create_window)
        self.patient_id.setGeometry(QtCore.QRect(90, 10, 151, 31))
        self.patient_id.setObjectName("patient_id")
        self.groupBox = QtWidgets.QGroupBox(patient_create_window)
        self.groupBox.setGeometry(QtCore.QRect(10, 170, 371, 91))
        self.groupBox.setObjectName("groupBox")
        self.patient_comments = QtWidgets.QPlainTextEdit(self.groupBox)
        self.patient_comments.setGeometry(QtCore.QRect(10, 20, 351, 64))
        self.patient_comments.setObjectName("patient_comments")
        self.save_btn = QtWidgets.QPushButton(patient_create_window)
        self.save_btn.setGeometry(QtCore.QRect(20, 270, 75, 23))
        self.save_btn.setObjectName("save_btn")
        self.cancel_btn = QtWidgets.QPushButton(patient_create_window)
        self.cancel_btn.setGeometry(QtCore.QRect(110, 270, 75, 23))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cancel_btn.sizePolicy().hasHeightForWidth())
        self.cancel_btn.setSizePolicy(sizePolicy)
        self.cancel_btn.setObjectName("cancel_btn")
        self.accident_date = QtWidgets.QDateEdit(patient_create_window)
        self.accident_date.setGeometry(QtCore.QRect(170, 80, 110, 22))
        self.accident_date.setObjectName("accident_date")

        self.retranslateUi(patient_create_window)
        QtCore.QMetaObject.connectSlotsByName(patient_create_window)

    def retranslateUi(self, patient_create_window):
        patient_create_window.setWindowTitle(QtWidgets.QApplication.translate("patient_create_window", "Dialog", None, -1))
        self.label.setText(QtWidgets.QApplication.translate("patient_create_window", "ID Pacjenta", None, -1))
        self.label_2.setText(QtWidgets.QApplication.translate("patient_create_window", "Data urodzenia (rok urodzenia)", None, -1))
        self.label_3.setText(QtWidgets.QApplication.translate("patient_create_window", "Data w wypadku", None, -1))
        self.label_4.setText(QtWidgets.QApplication.translate("patient_create_window", "Etiologia", None, -1))
        self.ethiology_main.setItemText(0, QtWidgets.QApplication.translate("patient_create_window", "TBI", None, -1))
        self.ethiology_main.setItemText(1, QtWidgets.QApplication.translate("patient_create_window", "nTBI", None, -1))
        self.ethiology_main.setItemText(2, QtWidgets.QApplication.translate("patient_create_window", "Inne (wpisz obok)", None, -1))
        self.groupBox.setTitle(QtWidgets.QApplication.translate("patient_create_window", "Uwagi", None, -1))
        self.save_btn.setText(QtWidgets.QApplication.translate("patient_create_window", "ZAPISZ", None, -1))
        self.cancel_btn.setText(QtWidgets.QApplication.translate("patient_create_window", "ODRZUĆ", None, -1))
        self.accident_date.setDisplayFormat(QtWidgets.QApplication.translate("patient_create_window", "MM/dd/yyyy", None, -1))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    patient_create_window = QtWidgets.QDialog()
    ui = Ui_patient_create_window()
    ui.setupUi(patient_create_window)
    patient_create_window.show()
    sys.exit(app.exec_())

