# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/dmj/fizmed/mdovgialo/crs_sheet/crs_sheet/resources/main_window.ui',
# licensing of '/dmj/fizmed/mdovgialo/crs_sheet/crs_sheet/resources/main_window.ui' applies.
#
# Created: Wed Oct 16 11:56:50 2019
#      by: pyside2-uic  running on PySide2 5.11.0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_main_window(object):
    def setupUi(self, main_window):
        main_window.setObjectName("main_window")
        main_window.resize(556, 418)
        self.gridLayout = QtWidgets.QGridLayout(main_window)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.create_patient_btn = QtWidgets.QPushButton(main_window)
        self.create_patient_btn.setObjectName("create_patient_btn")
        self.horizontalLayout_2.addWidget(self.create_patient_btn)
        self.patient_id_list_cmb = QtWidgets.QComboBox(main_window)
        self.patient_id_list_cmb.setObjectName("patient_id_list_cmb")
        self.horizontalLayout_2.addWidget(self.patient_id_list_cmb)
        self.edit_patient_btn = QtWidgets.QPushButton(main_window)
        self.edit_patient_btn.setObjectName("edit_patient_btn")
        self.horizontalLayout_2.addWidget(self.edit_patient_btn)
        self.delete_patient_btn = QtWidgets.QPushButton(main_window)
        self.delete_patient_btn.setObjectName("delete_patient_btn")
        self.horizontalLayout_2.addWidget(self.delete_patient_btn)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.create_btn = QtWidgets.QPushButton(main_window)
        self.create_btn.setObjectName("create_btn")
        self.horizontalLayout.addWidget(self.create_btn)
        self.edit_btn = QtWidgets.QPushButton(main_window)
        self.edit_btn.setObjectName("edit_btn")
        self.horizontalLayout.addWidget(self.edit_btn)
        self.delete_btn = QtWidgets.QPushButton(main_window)
        self.delete_btn.setObjectName("delete_btn")
        self.horizontalLayout.addWidget(self.delete_btn)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.records = QtWidgets.QTableWidget(main_window)
        self.records.setObjectName("records")
        self.records.setColumnCount(0)
        self.records.setRowCount(0)
        self.verticalLayout.addWidget(self.records)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.sort_by_id_checkbox = QtWidgets.QRadioButton(main_window)
        self.sort_by_id_checkbox.setObjectName("sort_by_id_checkbox")
        self.buttonGroup = QtWidgets.QButtonGroup(main_window)
        self.buttonGroup.setObjectName("buttonGroup")
        self.buttonGroup.addButton(self.sort_by_id_checkbox)
        self.horizontalLayout_3.addWidget(self.sort_by_id_checkbox)
        self.sort_by_date_checkbox = QtWidgets.QRadioButton(main_window)
        self.sort_by_date_checkbox.setObjectName("sort_by_date_checkbox")
        self.buttonGroup.addButton(self.sort_by_date_checkbox)
        self.horizontalLayout_3.addWidget(self.sort_by_date_checkbox)
        self.default_sort_checkbox = QtWidgets.QRadioButton(main_window)
        self.default_sort_checkbox.setChecked(True)
        self.default_sort_checkbox.setObjectName("default_sort_checkbox")
        self.buttonGroup.addButton(self.default_sort_checkbox)
        self.horizontalLayout_3.addWidget(self.default_sort_checkbox)
        self.export_csv_btn = QtWidgets.QPushButton(main_window)
        self.export_csv_btn.setObjectName("export_csv_btn")
        self.horizontalLayout_3.addWidget(self.export_csv_btn)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.retranslateUi(main_window)
        QtCore.QMetaObject.connectSlotsByName(main_window)

    def retranslateUi(self, main_window):
        main_window.setWindowTitle(QtWidgets.QApplication.translate("main_window", "CRS pomocnik", None, -1))
        self.create_patient_btn.setText(QtWidgets.QApplication.translate("main_window", "Stwórz pacjenta", None, -1))
        self.edit_patient_btn.setText(QtWidgets.QApplication.translate("main_window", "Edytuj pacjenta", None, -1))
        self.delete_patient_btn.setText(QtWidgets.QApplication.translate("main_window", "Usuń pacjenta", None, -1))
        self.create_btn.setText(QtWidgets.QApplication.translate("main_window", "Stwórz nowy wpis", None, -1))
        self.edit_btn.setText(QtWidgets.QApplication.translate("main_window", "Edytuj/podgląd", None, -1))
        self.delete_btn.setText(QtWidgets.QApplication.translate("main_window", "Usuń", None, -1))
        self.sort_by_id_checkbox.setText(QtWidgets.QApplication.translate("main_window", "Sortuj po ID pacjenta", None, -1))
        self.sort_by_date_checkbox.setText(QtWidgets.QApplication.translate("main_window", "Sortuj po dacie", None, -1))
        self.default_sort_checkbox.setText(QtWidgets.QApplication.translate("main_window", "Sortuj w kolejności dodawania", None, -1))
        self.export_csv_btn.setText(QtWidgets.QApplication.translate("main_window", "Export CSV", None, -1))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    main_window = QtWidgets.QDialog()
    ui = Ui_main_window()
    ui.setupUi(main_window)
    main_window.show()
    sys.exit(app.exec_())

