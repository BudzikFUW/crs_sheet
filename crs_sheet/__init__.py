import csv
import os
import socket
import datetime
from collections import defaultdict

from PySide2.QtGui import QWindow, QGuiApplication
from PySide2.QtWidgets import QApplication, QDialog, QMessageBox, QTableWidgetItem, QComboBox, QFileDialog
from PySide2.QtCore import QDate, QTime
import PySide2.QtCore

from crs_sheet.resources.create_patient_ui import Ui_patient_create_window
from .resources.main_window_ui import Ui_main_window
from .resources.patient_edit_ui import Ui_edit_record
import sqlite3


varnames = ['auditory', 'visual', 'motor', 'oromotor', 'communication', 'arousal']
ETHIOLOGY_INDEX_OTHER = 2
DIAGNOSIS_INDEX_OTHER = 6


def get_patient_list(db):
    c = db.cursor()
    record = [i[0] for i in list(c.execute('SELECT patient_id FROM patients;'))]
    c.close()
    return record


class Record:
    @classmethod
    def create_record_from_ui(cls, ui: Ui_edit_record, uid):
        self = cls()
        self.id = uid
        self.patient = ui.patient_id.currentText()

        self.datetime = datetime.datetime(ui.meas_date.date().year(),
                                          ui.meas_date.date().month(),
                                          ui.meas_date.date().day(),
                                          ui.meas_date.time().hour(),
                                          ui.meas_date.time().minute(),
                                          ui.meas_date.time().second(),
                                                   )
        dict_modalities = defaultdict(list)
        for var_name in varnames:
            for i in range(7):
                var = '{}{}'.format(var_name, i)
                checkbox = getattr(ui, var, False)
                if checkbox is not False:
                    checkbox = int(checkbox.isChecked())
                    setattr(self, var, checkbox)
                    dict_modalities[var_name].append(checkbox)
        for var_name in varnames:
            value = 0
            for i in reversed(range(7)):
                try:
                    if dict_modalities[var_name][i]:
                        value += i
                except IndexError:
                    pass
            setattr(self, var_name, value)

        self.diagnosis = ui.diagnosis.currentText()
        self._diagnosis_id = ui.diagnosis.currentIndex()
        self.diagnosis_custom = ui.diagnosis_custom.toPlainText()
        if self._diagnosis_id == DIAGNOSIS_INDEX_OTHER:
            self.diagnosis_final = self.diagnosis_custom
        else:
            self.diagnosis_final = self.diagnosis
        self.comment = ui.comment.toPlainText()
        self.researcher_id = ui.researcher_id.toPlainText()
        return self

    @staticmethod
    def fill_window_from_db(db, window: Ui_edit_record, record_id):
        c = db.cursor()

        record = list(c.execute('SELECT * FROM records WHERE "id"=?;', [record_id]))[0]
        column_names = [i[1] for i in c.execute('PRAGMA table_info(records);')]

        window.patient_id.setCurrentText(record[column_names.index('patient')])
        date_time_python = datetime.datetime.strptime(record[1], "%Y-%m-%d %H:%M:%S")
        date = QDate(date_time_python.year, date_time_python.month, date_time_python.day)
        time = QTime(date_time_python.hour, date_time_python.minute, date_time_python.second)
        window.meas_date.setDate(date)
        window.meas_date.setTime(time)

        for var_name in varnames:
            for i in range(7):
                var = '{}{}'.format(var_name, i)
                try:
                    check = bool(record[column_names.index(var)])
                except ValueError:
                    continue
                try:
                    checkbox = getattr(window, var)
                    checkbox.setChecked(check)
                except AttributeError:
                    pass

        diagnosis = record[column_names.index('diagnosis')]
        window.diagnosis.setCurrentText(diagnosis)
        diagnosis_custom = record[column_names.index('diagnosis_custom')]
        window.diagnosis_custom.setPlainText(diagnosis_custom)

        window.comment.setPlainText(record[column_names.index('comment')])
        window.researcher_id.setPlainText(record[column_names.index('researcher_id')])

        window.record_id = record_id

        c.close()


class Patient:
    def __init__(self, patient_id: str, birth_year: int, illness_start_date: datetime.datetime,
                 ethiology_main: str, ethiology_custom: str, comment: str):
        self.patient_id = patient_id
        self.birth_year = birth_year
        self.illness_start_date = illness_start_date
        self.ethiology_main = ethiology_main
        self.ethiology_custom = ethiology_custom
        if len(self.ethiology_main) == 0:
            self.ethiology_final = self.ethiology_custom
        else:
            self.ethiology_final = self.ethiology_main
        self.comment = comment

    @classmethod
    def from_db(cls, db, patient_id):
        c = db.cursor()
        data = list(c.execute('select * from patients where "patient_id"=?', [patient_id, ]))[0]
        illness_datetime = datetime.datetime.strptime(data[2], "%Y-%m-%d %H:%M:%S")
        record = cls(data[0], data[1], illness_datetime, data[3], data[4], data[6])

        c.close()
        return record

    def write_to_db(self, db):
        c = db.cursor()
        column_names = [i[1] for i in c.execute('PRAGMA table_info(patients);')]

        record_exists = len(list(c.execute('SELECT * FROM patients WHERE "patient_id"=?;', [self.patient_id, ]))) > 0
        if record_exists:
            for column_name in column_names:
                c.execute('UPDATE patients  SET "{}" = ? WHERE "patient_id"=?;'.format(column_name),
                          [getattr(self, column_name), self.patient_id])
        else:
            sql = '''INSERT INTO patients ({}) VALUES({})'''.format(','.join(column_names), ('?,' * len(column_names)).rstrip(','))
            values = []
            for i in column_names:
                values.append(getattr(self, i))
            c.execute(sql, values)
        c.close()
        db.commit()

    def fill_window(self, window: Ui_patient_create_window):
        window.patient_id.setPlainText(self.patient_id)
        window.birth_date.setValue(self.birth_year)
        qdate = QDate(self.illness_start_date.year, self.illness_start_date.month, self.illness_start_date.day)
        window.accident_date.setDate(qdate)
        if self.ethiology_main:
            window.ethiology_main.setCurrentText(self.ethiology_main)
            window.ethiology_custom.setPlainText('')
            window.ethiology_custom.setEnabled(False)
        else:
            window.ethiology_main.setCurrentIndex(ETHIOLOGY_INDEX_OTHER)
            window.ethiology_custom.setPlainText(self.ethiology_custom)
            window.ethiology_custom.setEnabled(True)
        window.patient_comments.setPlainText(self.comment)


    @classmethod
    def from_window(cls, window: Ui_patient_create_window):
        patient_id = window.patient_id.toPlainText()
        birth_year = window.birth_date.value()
        qdate = window.accident_date.date()
        illness_start_date = datetime.datetime(qdate.year(), qdate.month(), qdate.day())
        if window.ethiology_main.currentIndex() == ETHIOLOGY_INDEX_OTHER:
            ethiology_main = ''
            ethiology_custom = window.ethiology_custom.toPlainText()
        else:
            ethiology_main = window.ethiology_main.currentText()
            ethiology_custom = ''
        comment = window.patient_comments.toPlainText()
        return cls(patient_id, birth_year, illness_start_date, ethiology_main, ethiology_custom, comment)


class AppMainWindow(QDialog):

    def set_show_callback(self, callback):
        self.show_callback = callback

    def show(self):
        try:
            self.show_callback()
        except AttributeError:
            pass
        super().show()


class App:
    def __init__(self):
        self._db_path = '~/crs_patients_{}.db'.format(socket.gethostname())
        self._db_conn = sqlite3.connect(os.path.abspath(os.path.expanduser(self._db_path)))
        self._create_table()
        ui = Ui_main_window()
        self._main_window = AppMainWindow()

        ui.setupUi(self._main_window)
        self._main_window_items = ui
        self._edit = QDialog()
        uie = Ui_edit_record()
        uie.setupUi(self._edit)
        self._edit_items = uie

        self._main_window_items.create_btn.pressed.connect(self.create_pressed)
        self._main_window_items.delete_btn.pressed.connect(self.delete_pressed)
        self._main_window_items.edit_btn.pressed.connect(self.edit_pressed)

        self._main_window_items.create_patient_btn.pressed.connect(self.create_patient_pressed)
        self._main_window_items.delete_patient_btn.pressed.connect(self.delete_patient_pressed)
        self._main_window_items.edit_patient_btn.pressed.connect(self.edit_patient_pressed)

        self._edit_items.save.pressed.connect(self.save_pressed)
        self._edit_items.cancel.pressed.connect(self.dont_save_pressed)

        self._edit_patient_window = QDialog()
        self._edit_patient_window_items = Ui_patient_create_window()
        self._edit_patient_window_items.setupUi(self._edit_patient_window)

        self._edit_patient_window_items.save_btn.pressed.connect(self.save_patient_pressed)
        self._edit_patient_window_items.cancel_btn.pressed.connect(self.dont_save_patient_pressed)

        self._main_window.set_show_callback(self.recalculate_list_view)
        self._main_window_items.sort_by_date_checkbox.released.connect(self._ordering_slot)
        self._main_window_items.sort_by_id_checkbox.released.connect(self._ordering_slot)
        self._main_window_items.default_sort_checkbox.released.connect(self._ordering_slot)

        self._edit_items.diagnosis.currentIndexChanged.connect(self.diagnosis_selected_callback)
        self._edit_patient_window_items.ethiology_main.currentIndexChanged.connect(self.ethiology_selected_callback)

        self._populate_patients_combobox(self._main_window_items.patient_id_list_cmb)

        self._bind_checkboxes_to_update_summary()

        self._main_window_items.export_csv_btn.pressed.connect(self._export_to_csv_pressed)

    def _export_to_csv_pressed(self):
        path = QFileDialog.getSaveFileName()[0]
        c = self._db_conn.cursor()
        column_names = [i[1] for i in c.execute('PRAGMA table_info(full_records);')]
        data = c.execute("SELECT * FROM full_records;")

        with open(path, 'w') as f:
            writer = csv.writer(f)
            writer.writerow(column_names)
            writer.writerows(data)

        c.close()


    def _ordering_slot(self):
        self._main_window.show()

    def _bind_checkboxes_to_update_summary(self):
        for var in varnames:
            for i in range(10):
                try:
                    item = getattr(self._edit_items, '{}{}'.format(var, i))
                    item.stateChanged.connect(self._update_summary)
                except AttributeError:
                    pass

    def _update_summary(self, x):
        values = defaultdict(int)
        for var in varnames:
            for i in range(10):
                try:
                    item = getattr(self._edit_items, '{}{}'.format(var, i))
                    values[var] += int(item.isChecked()) * i
                except AttributeError:
                    pass
        total = sum(values.values())
        summary_s = ''
        for var in varnames:
            substring = '{}: {}\n'.format(var.capitalize(), values[var])
            summary_s += substring
        substring = '{}: {}\n'.format("Ogółem", total)
        summary_s += substring

        self._edit_items.summary.setText(summary_s)

    def _get_sorting_string(self):
        if self._main_window_items.sort_by_date_checkbox.isChecked():
            # sortuj po dacie a potem po id
            return 'ORDER BY datetime, patient'
        if self._main_window_items.sort_by_id_checkbox.isChecked():
            # sortuj po id a potem po dacie
            return 'ORDER BY patient, datetime'
        else:
            # brak sortowania
            return 'ORDER BY "id"'

    def recalculate_list_view(self):
        self._populate_patients_combobox(self._main_window_items.patient_id_list_cmb)
        c = self._db_conn.cursor()
        table = self._main_window_items.records
        table.clear()

        sorting = self._get_sorting_string()

        records = list(c.execute('SELECT "id", patient, datetime, diagnosis_final, total_score FROM full_records {};'.format(sorting)))
        table.setRowCount(len(records))
        if not len(records):
            return

        table.setColumnCount(len(records[0]))
        table.setHorizontalHeaderLabels(["ID Wpisu", "ID pacjenta", "data", "diagnoza", "punkty"])

        for nr, record in enumerate(records):
            for col_nr, value in enumerate(record):
                item = QTableWidgetItem(str(value))
                item.setFlags(item.flags() & ~PySide2.QtCore.Qt.ItemIsEditable)
                table.setItem(nr, col_nr, item)
        c.close()

    def _create_table(self):
        c = self._db_conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS records
                    (id INTEGER PRIMARY KEY,
                     datetime DATETIME,
                     patient TEXT,
                     auditory4 INTEGER,
                     auditory3 INTEGER,
                     auditory2 INTEGER,
                     auditory1 INTEGER,
                     auditory0 INTEGER,
                     auditory INTEGER,
                     visual5 INTEGER,
                     visual4 INTEGER,
                     visual3 INTEGER,
                     visual2 INTEGER,
                     visual1 INTEGER,
                     visual0 INTEGER,
                     visual INTEGER,
                     motor6 INTEGER,
                     motor5 INTEGER,
                     motor4 INTEGER,
                     motor3 INTEGER,
                     motor2 INTEGER,
                     motor1 INTEGER,
                     motor0 INTEGER,
                     motor INTEGER,
                     oromotor3 INTEGER,
                     oromotor2 INTEGER,
                     oromotor1 INTEGER,
                     oromotor0 INTEGER,
                     oromotor INTEGER,
                     communication2 INTEGER,
                     communication1 INTEGER,
                     communication0 INTEGER,
                     communication INTEGER,
                     arousal3 INTEGER,
                     arousal2 INTEGER,
                     arousal1 INTEGER,
                     arousal0 INTEGER,
                     arousal INTEGER,
                     diagnosis TEXT,
                     diagnosis_custom TEXT,
                     diagnosis_final TEXT,
                     researcher_id TEXT,
                     comment TEXT);''')

        c.execute('''CREATE TABLE IF NOT EXISTS patients
                     (patient_id TEXT PRIMARY KEY,
                     birth_year INTEGER,
                     illness_start_date DATE,
                     ethiology_main TEXT,
                     ethiology_custom TEXT,
                     ethiology_final TEXT,
                     comment TEXT);''')

        c.execute('''DROP VIEW IF EXISTS full_records;''')
        c.execute('''CREATE VIEW full_records
                     AS
                     SELECT
                        records.id,
                        records.datetime,    
                        patient,  
                        patients.birth_year,
                        patients.illness_start_date,       
                        patients.ethiology_main,       
                        patients.ethiology_custom,       
                        patients.ethiology_final,       
                        patients.comment as 'patient_comment',       
                        auditory4,    
                        auditory3,  
                        auditory2,    
                        auditory1,    
                        auditory0,    
                        auditory,     
                        visual5,      
                        visual4,      
                        visual3,      
                        visual2,      
                        visual1,      
                        visual0,      
                        visual,       
                        motor6,       
                        motor5,       
                        motor4,       
                        motor3,       
                        motor2,       
                        motor1,       
                        motor0,       
                        motor,        
                        oromotor3,    
                        oromotor2,    
                        oromotor1,    
                        oromotor0,    
                        oromotor,     
                        communication2,
                        communication1,
                        communication0,
                        communication,
                        arousal3,     
                        arousal2,     
                        arousal1,     
                        arousal0,     
                        arousal,
                        (auditory + visual + motor + oromotor + communication + arousal) as 'total_score',
                        diagnosis,       
                        diagnosis_custom,
                        diagnosis_final,
                        researcher_id,   
                        records.comment as 'record_comment'
                     FROM
                        records
                     INNER JOIN
                        patients ON patients.patient_id = records.patient;
                     
                        ''')

        c.close()

    def _populate_patients_combobox(self, combobox: QComboBox):
        patients = get_patient_list(self._db_conn)
        combobox.clear()
        combobox.addItems(patients)

    def reset_edit_window(self):
        now = datetime.datetime.now()
        date = QDate(now.year, now.month, now.day)
        self._edit_items.meas_date.setDate(date)
        time_now = QTime(now.hour, now.minute, now.second)
        self._edit_items.meas_date.setTime(time_now)
        try:
            del self._edit_items.record_id
        except AttributeError:
            pass

        window = self._edit_items

        self._populate_patients_combobox(window.patient_id)

        for var_name in varnames:
            for i in range(7):
                var = '{}{}'.format(var_name, i)

                try:
                    checkbox = getattr(window, var)
                    checkbox.setChecked(False)
                except AttributeError:
                    pass

        window.diagnosis.setCurrentText("UWS")
        window.diagnosis_custom.setPlainText("")
        window.comment.setPlainText("")

    def fill_edit_window(self, record_id):
        if record_id is None:
            self.reset_edit_window()
        else:
            self._populate_patients_combobox(self._edit_items.patient_id)
            Record.fill_window_from_db(self._db_conn, self._edit_items, record_id)
        self._update_summary(None)

    def fill_record(self):
        c = self._db_conn.cursor()
        try:
            id = self._edit_items.record_id
        except AttributeError:
            try:
                last_id = list(c.execute('SELECT "id" FROM records ORDER BY "id" DESC LIMIT 1;'))[0][0]
                id = last_id + 1
            except IndexError:
                id = 0

        record = Record.create_record_from_ui(self._edit_items, id)

        column_names = [i[1] for i in c.execute('PRAGMA table_info(records);')]

        record_exists = len(list(c.execute('SELECT * FROM records WHERE "id"={};'.format(record.id)))) > 0
        if record_exists:
            for column_name in column_names:
                c.execute('UPDATE records SET "{}" = ? WHERE "id"={};'.format(column_name, record.id), [getattr(record, column_name)])
        else:
            sql = '''INSERT INTO records({}) VALUES({})'''.format(','.join(column_names), ('?,' * len(column_names)).rstrip(','))
            values = []
            for i in column_names:
                values.append(getattr(record, i))

            c.execute(sql, values)

        c.close()
        self._db_conn.commit()

    def main(self):
        self._main_window.show()

    def diagnosis_selected_callback(self, index):
        self._edit_items.diagnosis_custom.setEnabled(int(index) == DIAGNOSIS_INDEX_OTHER)
        if int(index) != DIAGNOSIS_INDEX_OTHER:
            self._edit_items.diagnosis_custom.setPlainText("")

    def ethiology_selected_callback(self, index):
        self._edit_patient_window_items.ethiology_custom.setEnabled(int(index) == ETHIOLOGY_INDEX_OTHER)
        if int(index) != DIAGNOSIS_INDEX_OTHER:
            self._edit_patient_window_items.ethiology_custom.setPlainText("")

    def create_pressed(self):
        self._main_window.hide()
        self.fill_edit_window(None)
        self._edit_items.patient_id.setEnabled(True)
        self._edit.show()

    def get_selected_row_id(self):
        selected_items = list(self._main_window_items.records.selectedItems())
        if len(selected_items) > 1:
            QMessageBox.warning(self._main_window, "Wybierz jedne pole", "Wybierz jedne pole do edytowania")
            return
        elif len(selected_items) == 0:
            return
        item = selected_items[0]
        id = int(self._main_window_items.records.item(item.row(), 0).text())
        return id

    def edit_pressed(self):
        id = self.get_selected_row_id()
        if id is not None:
            self._main_window.hide()
            self.fill_edit_window(id)
            self._edit_items.patient_id.setEnabled(False)
            self._edit.show()

    def delete_pressed(self):
        id = self.get_selected_row_id()
        if id is None:
            return
        ans = QMessageBox.question(self._main_window, "Potwierdzenie", "Na pewno usunąć")
        if ans == QMessageBox.StandardButton.No:
            return
        else:
            sql = 'DELETE FROM records WHERE id=?'
            cur = self._db_conn.cursor()
            cur.execute(sql, (id,))
            self._db_conn.commit()
            self._main_window.show()

    def dont_save_pressed(self):
        self._main_window.show()
        self._edit.hide()

    def validate_edit_window(self):
        patient_id = self._edit_items.patient_id.currentText()
        if len(patient_id) == 0:
            QMessageBox.warning(self._edit, "Błąd", "Nie podano ID pacjenta!")
            return False
        return True

    def _validate_patient_edit_window(self, edit=False):
        patient_id = self._edit_patient_window_items.patient_id.toPlainText()
        if len(patient_id) == 0:
            QMessageBox.warning(self._edit, "Błąd", "Nie podano ID pacjenta!")
            return False

        existing_patients = get_patient_list(self._db_conn)
        if patient_id in existing_patients:
            ans = QMessageBox.question(self._edit, "Błąd", "Pacjent {} już istnieje! Nadpisać?".format(patient_id))
            if ans == QMessageBox.No:
                return False

        return True

    def save_pressed(self):
        if not self.validate_edit_window():
            return
        self.fill_record()
        self._main_window.show()
        self._edit.hide()

    def create_patient_pressed(self):
        self._main_window.hide()
        patient = Patient('', 2000, datetime.datetime(2000, 1, 1), 'TBI', '', '')
        patient.fill_window(self._edit_patient_window_items)
        self._edit_patient_window_items.patient_id.setEnabled(True)
        self._edit_patient_window.show()

    def edit_patient_pressed(self):
        patient_id = self._main_window_items.patient_id_list_cmb.currentText()
        if patient_id:
            self._main_window.hide()
            patient = Patient.from_db(self._db_conn, patient_id)
            patient.fill_window(self._edit_patient_window_items)
            self._edit_patient_window_items.patient_id.setEnabled(False)
            self._edit_patient_window.show()

    def delete_patient_pressed(self):
        id = self._main_window_items.patient_id_list_cmb.currentText()
        if id is None:
            return
        ans = QMessageBox.question(self._main_window, "Potwierdzenie", "Na pewno usunąć pacjenta {} i wszystkie pomiary z nim związane?".format(id))
        if ans == QMessageBox.StandardButton.No:
            return
        else:
            sql = 'DELETE FROM records WHERE patient=?'
            cur = self._db_conn.cursor()
            cur.execute(sql, (id,))
            self._db_conn.commit()

            sql = 'DELETE FROM patients WHERE patient_id=?'
            cur = self._db_conn.cursor()
            cur.execute(sql, (id,))
            self._db_conn.commit()
        self._main_window.show()

    def save_patient_pressed(self):
        if not self._validate_patient_edit_window():
            return

        self._edit_patient_window.hide()
        patient = Patient.from_window(self._edit_patient_window_items)
        patient.write_to_db(self._db_conn)
        self._main_window.show()

    def dont_save_patient_pressed(self):
        self._edit_patient_window.hide()
        self._main_window.show()

def main():
    qapp = QApplication([])
    app = App()
    app.main()
    qapp.exec_()